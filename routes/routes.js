const express = require('express');
const router = express.Router();

const product = require('../controller/product');

//GET methods
router.get('/',function(req,res){
	res.send("Welcome");
})
router.get('/test',product.test);

//Create
router.post('/create',product.create);

//Read
router.get('/:id',product.get_product);

//Update
router.put('/:id/update',product.update);

//delete
router.delete('/:id/delete',product.delete);


module.exports = router;