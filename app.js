const express = require('express')
const bodyParser= require('body-parser')

const routes= require('./routes/routes');

const app = express();

// Set up mongoose connection
const mongoose = require('mongoose');
let dev_db_url = 'mongodb://saroj:saroj12345@ds155091.mlab.com:55091/zobfindr';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));



app.use('/products',routes);

//creating port number

let port = 3000;

app.listen(port,() =>{
	console.log('listening on port '+ port);
})
