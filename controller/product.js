const Product = require('../models/product_model');

//Test
exports.test=function(req,res){
	res.send("welcome, test successful");
};

//Create
exports.create=(req,res)=>{
	let product = new Product(
		{
			name:req.body.name,
			price:req.body.price
		}
	);

	product.save(function (err,product){
		if(err){
			console.log(err);
			return res.status(500).send();
		}else{
		res.send(product+' created successfully')
		}
	})
};


//Read
exports.get_product=(req,res)=>{
	Product.findById(req.params.id,function(err,product){
		if(err){
			console.log(err);
			return res.status(500).send();
		}
		res.send(product);
	})
};

//Update
exports.update=(req,res)=>{
	Product.findByIdAndUpdate(req.params.id,{$set: req.body},function(err,product){
		if(err){
			console.log(err);
			return res.status(500).send();
		}
		res.send(product+ 'updated');
	});
}


//Delete
exports.delete=(req,res)=>{
	Product.findByIdAndRemove(req.params.id,function(err,product) {
		if(err){
			console.log(err);
			return res.status(500).send();
		}
		res.send(product+ 'removed form database');
	});
}